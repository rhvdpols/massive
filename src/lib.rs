extern crate chrono;
extern crate serde;

mod log;
mod messaging;

pub fn run() {
    let _log_writer = log::Writer::new();
    {
        let _unused_logger = log::Logger::new().ok();
    }
    let mut logger = log::Logger::new().ok();
    {
        let mut logger2 = log::Logger::new().ok();

        for i in 1..6 {
            if let Some(logger) = logger.as_mut() {
                logger.log(format!("Logger 1 msg {}", i)).unwrap();
            }
            if let Some(logger2) = logger2.as_mut() {
                logger2.log(format!("Logger 2 msg {}", i)).unwrap();
            }
        }
    }
    let mut logger2 = log::Logger::new().ok();

    for i in 1..6 {
        if let Some(logger) = logger.as_mut() {
            logger.log(format!("Logger 1 msg {}", i + 5)).unwrap();
        }
        if let Some(logger2) = logger2.as_mut() {
            logger2.log(format!("Logger 3 msg {}", i)).unwrap();
        }
    }
}
