use std::io::prelude::*;
use std::net::TcpStream;

use serde::{Deserialize, Serialize};

use crate::messaging;

#[derive(Serialize, Deserialize)]
pub enum Action {
    Log,
    Exit,
}

#[derive(Serialize, Deserialize)]
pub struct Message {
    pub action: Action,
    pub text: String,
}

pub fn send_message(stream: &mut TcpStream, msg: Message) -> Result<(), messaging::Error> {
    // Serialize message, get length of message in byte array
    //
    let serialized = serde_json::to_string(&msg)?;
    let len = serialized.len() as u32;
    let len_bytes = len.to_be_bytes();

    // Write message length, then message
    //
    stream.write_all(&len_bytes)?;
    stream.write_all(serialized.as_bytes())?;

    Ok(())
}

pub fn receive_message(stream: &mut TcpStream) -> Result<Message, messaging::Error> {
    // Read first 4 bytes, u32 with message length
    //
    let mut len_buf = [0; 4];
    let read = stream.read(&mut len_buf)?;

    if read == 0 {
        return Err(messaging::Error {
            details: "sender closed the connection".to_string(),
            kind: std::io::ErrorKind::NotConnected,
        });
    } else if read < 4 {
        panic!("read only {} bytes", read);

        // TODO block to finish reading this message
    }

    let len: u32 = u32::from_be_bytes(len_buf);

    // Create buffer with exact length of message, read message
    //
    let mut buf: Vec<u8> = vec![0; len as usize];
    stream.read_exact(buf.as_mut_slice())?;

    // Deserialize
    let msg = serde_json::from_slice::<Message>(&buf)?;

    Ok(msg)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::net::TcpListener;
    use std::thread;

    #[test]
    fn message_send_and_receive() {
        // bind listener
        let listener = TcpListener::bind("localhost:2345").unwrap();

        thread::spawn(move || {
            // connect
            let mut client = TcpStream::connect("localhost:2345").unwrap();

            send_message(
                &mut client,
                Message {
                    action: Action::Log,
                    text: String::from("hallo dit is een bericht"),
                },
            )
            .unwrap();
            send_message(
                &mut client,
                Message {
                    action: Action::Log,
                    text: String::from("hallo dit is ook een bericht"),
                },
            )
            .unwrap();
            send_message(
                &mut client,
                Message {
                    action: Action::Log,
                    text: String::from("the next message will be the exit command, l8rs"),
                },
            )
            .unwrap();
            send_message(
                &mut client,
                Message {
                    action: Action::Exit,
                    text: String::from("i just told the receiving end to exit!"),
                },
            )
            .unwrap();
        });

        let (mut server, _addr) = listener.accept().unwrap();

        assert_eq!(
            receive_message(&mut server).unwrap().text,
            "hallo dit is een bericht"
        );
        assert_eq!(
            receive_message(&mut server).unwrap().text,
            "hallo dit is ook een bericht"
        );
        assert_eq!(
            receive_message(&mut server).unwrap().text,
            "the next message will be the exit command, l8rs"
        );
        assert_eq!(
            receive_message(&mut server).unwrap().text,
            "i just told the receiving end to exit!"
        );

        println!("Receiving thread exited");
    }

    #[test]
    fn message_receive_should_not_block() {
        let listener = TcpListener::bind("localhost:2346").expect("failed to bind listener");
        let handle = thread::spawn(move || {
            // connect
            let (stream, _addr) = listener
                .accept()
                .expect("listener failed to accept connection");
            stream
                .set_nonblocking(true)
                .expect("failure setting stream to non-blocking");
            stream
        });

        let mut _client =
            TcpStream::connect("localhost:2346").expect("client failed to connect to listener");
        let mut server = handle.join().expect("failed to get listener from thread");

        match receive_message(&mut server) {
            Ok(_msg) => panic!("can't possibly have received a message"),
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => (),
            Err(e) => panic!("{}", e),
        }
    }

    #[test]
    fn message_sender_closed_connection() {
        let listener = TcpListener::bind("localhost:2347").expect("failed to bind listener");
        let handle = thread::spawn(move || {
            // connect
            let (stream, _addr) = listener
                .accept()
                .expect("listener failed to accept connection");
            stream
                .set_nonblocking(true)
                .expect("failure setting stream to non-blocking");
            stream
        });

        let mut client =
            TcpStream::connect("localhost:2347").expect("client failed to connect to listener");
        let mut server = handle.join().expect("failed to get listener from thread");

        send_message(
            &mut client,
            Message {
                action: Action::Log,
                text: "this is a meesag".to_string(),
            },
        )
        .expect("sending a message failed");

        std::mem::drop(client);

        assert_eq!(
            receive_message(&mut server)
                .expect("receive message failed")
                .text,
            "this is a meesag".to_string()
        );

        // TODO find out what the correct error would be!
        match receive_message(&mut server) {
            Ok(_) => panic!("could not have received message"),
            Err(ref e) if e.kind() == std::io::ErrorKind::NotConnected => (),
            Err(e) => panic!("receive_message returned Err >>> {}", e),
        }
    }
}
