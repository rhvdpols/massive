use std::error;
use std::fmt;

#[derive(Debug)]
pub struct Error {
    pub details: String,
    pub kind: std::io::ErrorKind,
}

impl Error {
    pub fn kind(&self) -> std::io::ErrorKind {
        self.kind
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error {
            details: err.to_string(),
            kind: err.kind(),
        }
    }
}

impl From<serde_json::error::Error> for Error {
    fn from(err: serde_json::error::Error) -> Self {
        Error {
            details: err.to_string(),
            kind: std::io::ErrorKind::Other,
        }
    }
}
