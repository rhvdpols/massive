use std::net::TcpStream;

use chrono::prelude::*;

use crate::messaging;

// Logger might become a Trait in the future,
// currently I'm considering a builder pattern
// to add owner metadata
//
pub struct Logger {
    out: TcpStream,
}

impl Logger {
    pub fn new() -> Result<Logger, std::io::Error> {
        let stream = TcpStream::connect("localhost:2345")?;

        Ok(Logger { out: stream })
    }

    pub fn log(&mut self, msg: String) -> Result<(), messaging::Error> {
        let utc: DateTime<Utc> = Utc::now();

        let msg = messaging::Message {
            action: messaging::Action::Log,
            text: format!("{} - {}\n", utc, msg),
        };

        messaging::send_message(&mut self.out, msg)?;

        Ok(())
    }
}

// TODO needs tests
//
