use std::fs::{create_dir_all, OpenOptions};
use std::io::prelude::*;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::sync::mpsc::channel;
use std::thread;

use chrono::prelude::*;

use crate::messaging;

struct Connection {
    stream: TcpStream,
    addr: SocketAddr,
    alive: bool,
}

pub struct Writer {
    listener_handle: Option<thread::JoinHandle<()>>,
    stream_reader_handle: Option<thread::JoinHandle<()>>,
    file_thread_handle: Option<thread::JoinHandle<()>>,
}

impl Writer {
    pub fn new() -> Writer {
        let (stream_sender, stream_receiver) = channel::<(TcpStream, SocketAddr)>();
        let (string_sender, string_receiver) = channel::<String>();
        let (exit_sender, exit_receiver) = channel::<()>();

        // LogWriter TcpListener thread, listens for incoming TCP connections
        //
        let listener_handle = thread::Builder::new()
            .name("log_listener_thread".to_string())
            .spawn(move || {
                // bind a listener to the connection-string
                //
                let mut run = true;
                let listener = TcpListener::bind("localhost:2345").unwrap();
                listener.set_nonblocking(true).unwrap();

                while run {
                    if let Ok((server, addr)) = listener.accept() {
                        stream_sender.send((server, addr)).unwrap();
                    }

                    if exit_receiver.try_recv().is_ok() {
                        run = false;
                    }
                }
            });

        // LogWriter TcpStream reader thread, because spawning a thread for
        // each TcpStream is too easy
        //
        let stream_reader_handle = thread::Builder::new()
            .name("stream_reader_thread".to_string())
            .spawn(move || {
                let mut streams = Vec::new();
                let mut run = true;

                // send startup message
                //
                let utc: DateTime<Utc> = Utc::now();
                string_sender
                    .send(format!("===================================\nStarting up new log-writer instance\n{}\n", utc))
                    .unwrap();

                // TODO break loop when we receive exit signal
                //
                while run || !streams.is_empty() {
                    // accept a new stream if there is one
                    //
                    if let Ok((stream, addr)) = stream_receiver.try_recv() {
                        string_sender
                            .send(format!("{} new connection\n", addr))
                            .unwrap();
                        stream.set_nonblocking(true).unwrap();
                        streams.push(Connection {
                            stream,
                            addr,
                            alive: true,
                        });
                    }

                    // iterate over all streams to check for messages
                    //
                    for stream in streams.iter_mut() {
                        let msg = messaging::receive_message(&mut stream.stream);

                        match msg {
                            Ok(msg) => {
                                let text: String;

                                match msg.action {
                                    messaging::Action::Log => {
                                        text = format!("{} LOG: {}", stream.addr, msg.text)
                                    }
                                    messaging::Action::Exit => {
                                        text = format!("{} EXIT: {}", stream.addr, msg.text);
                                        run = false;
                                    }
                                }

                                string_sender.send(text).unwrap();
                            }
                            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                                // string_sender
                                //     .send(format!("{} {}\n", stream.addr, e))
                                //     .unwrap()
                            }
                            Err(ref e) if e.kind() == std::io::ErrorKind::NotConnected => {
                                string_sender
                                    .send(format!("{} {}\n", stream.addr, e))
                                    .unwrap();
                                stream.alive = false;
                            }
                            Err(_) => (),
                        }
                    }
                    streams.retain(|stream| stream.alive);
                }

                // send shutdown message
                //
                exit_sender.send(()).unwrap();

                let utc: DateTime<Utc> = Utc::now();
                string_sender
                    .send(format!("\nShutting down log-writer instance\n{}\n===================================\n\n", utc))
                    .unwrap();
            });

        // LogWriter file thread opens the log file, does all the writes
        //
        // if it doesn't exist, create the logs folder
        // then open the log.txt file
        // TODO make path configurable
        //
        let file_thread_handle = thread::Builder::new()
            .name("log_writer_file_thread".to_string())
            .spawn(move || {
                create_dir_all(".\\logs").unwrap();
                let mut file = OpenOptions::new()
                    .create(true)
                    .append(true)
                    .open(".\\logs\\log.txt")
                    .unwrap();

                for message in string_receiver.iter() {
                    file.write_all(message.as_bytes()).unwrap();
                    print!("{}", message);
                }
            });
        Writer {
            listener_handle: Some(listener_handle.unwrap()),
            stream_reader_handle: Some(stream_reader_handle.unwrap()),
            file_thread_handle: Some(file_thread_handle.unwrap()),
        }
    }
}

impl Drop for Writer {
    fn drop(&mut self) {
        // we want to send exit to the threads
        //
        {
            let mut stream = TcpStream::connect("localhost:2345").unwrap();
            messaging::send_message(
                &mut stream,
                messaging::Message {
                    action: messaging::Action::Exit,
                    text: "dropping LogWriter\n".to_string(),
                },
            )
            .unwrap();
        }

        // wait for all threads to finish
        //
        let handle = self.listener_handle.take().unwrap();
        handle.join().unwrap();

        let handle = self.stream_reader_handle.take().unwrap();
        handle.join().unwrap();

        let handle = self.file_thread_handle.take().unwrap();
        handle.join().unwrap();
    }
}

// TODO needs tests
//
